# -*- coding: utf-8 -*-

import sys                                                           # getdefaultencoding, getfilesystemencoding, platform, argv
import os                                                            # path, listdir
import tempfile                                                      # NamedTemporaryFile
import time                                                          # strftime
import datetime                                                      # datetime
import re                                                            # match, compile, sub
import logging, logging.handlers                                     # FileHandler, Formatter, getLogger, DEBUG | RotatingFileHandler
import inspect                                                       # getfile, currentframe
import ssl                                                           # SSLContext
import zipfile                                                       # ZipFile, namelist
import json                                                          # loads

from lxml import etree                                               # fromstring

# Plex imports
import Media                                                         # Episode
import VideoFiles                                                    # Scan
import Stack                                                         # Scan

try:                 
  from ssl import PROTOCOL_TLS as SSL_PROTOCOL  # Python >= 2.7.13 #ssl.PROTOCOL_TLSv1
except ImportError:
  from ssl import PROTOCOL_SSLv23 as SSL_PROTOCOL  # Python <  2.7.13
try:
  from urllib.request import urlopen, Request  # Python >= 3.0
except ImportError:
  from urllib2 import urlopen, Request  # Python == 2.x

### http://www.zytrax.com/tech/web/regex.htm  # http://regex101.com/#python
def com(string):  return re.compile(string)                 #RE Compile
def cic(string):  return re.compile(string, re.IGNORECASE)  #RE Compile Ignore Case

### Log variables, regex, skipped folders, words to remove, character maps ###
SetupDone        = False
Log              = None
Handler          = None
ScudLeeMappings  = None
PLEX_ROOT        = ""
PLEX_LIBRARY     = {}
PLEX_LIBRARY_URL = "http://127.0.0.1:32400/library/sections/"  # Allow to get the library name to get a log per library https://support.plex.tv/hc/en-us/articles/204059436-Finding-your-account-token-X-Plex-Token

ANIDB_TVDB_MAPPING = 'https://rawgit.com/ScudLee/anime-lists/master/anime-list-master.xml'

SSL_CONTEXT = ssl.SSLContext(SSL_PROTOCOL)
HEADERS     = {'Content-type': 'application/json'}

# Uses re.match() so forces a '^'
IGNORE_DIRS_RX_RAW  = [ '@Recycle', r'\.@__thumb', r'lost\+found', r'\.AppleDouble', r'\$Recycle.Bin', 'System Volume Information', 'Temporary Items', 'Network Trash Folder',   ###### Ignored folders
                        '@eaDir', 'Extras', r'Samples?', 'bonus', r'.*bonus disc.*', r'trailers?', r'.*_UNPACK_.*', r'.*_FAILED_.*', r'_?Misc', '.xattr']                        # source: Filters.py  removed '\..*',
IGNORE_DIRS_RX      = [cic(entry) for entry in IGNORE_DIRS_RX_RAW]

# https://regex101.com/r/aOYd6F/3
VALID_FILE_REGEX = r"\[(?P<subber>.+)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>(?:S?[0-9]{1,4}(?:\.5|[A-Z])?\-?){1,2}))?\s\[(?P<resolution>\d+p)\].(?P<extension>.+)"

### Setup core variables ################################################################################
def setup():
  global SetupDone
  if SetupDone:
    return
  
  SetupDone = True
  
  ### Define PLEX_ROOT ##################################################################################
  global PLEX_ROOT
  PLEX_ROOT = os.path.abspath(os.path.join(os.path.dirname(inspect.getfile(inspect.currentframe())), "..", ".."))
  if not os.path.isdir(PLEX_ROOT):
    path_location = { 'Windows': '%LOCALAPPDATA%\\Plex Media Server',
                      'MacOSX':  '$HOME/Library/Application Support/Plex Media Server',
                      'Linux':   '$PLEX_HOME/Library/Application Support/Plex Media Server' }
    PLEX_ROOT = os.path.expandvars(path_location[Platform.OS.lower()] if Platform.OS.lower() in path_location else '~')  # Platform.OS:  Windows, MacOSX, or Linux
  
  ### Define logging setup ##############################################################################
  global Log
  Log = logging.getLogger('main')
  Log.setLevel(logging.DEBUG)
  set_logging()
  
  ### Populate PLEX_LIBRARY #############################################################################
  Log.info("".ljust(157, '='))
  Log.info("Plex scan start: {}".format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f")))
  if os.path.isfile(os.path.join(PLEX_ROOT, "X-Plex-Token.id")):
    Log.info("'X-Plex-Token.id' file present")
    url = PLEX_LIBRARY_URL + "?X-Plex-Token=" + read_file(os.path.join(PLEX_ROOT, "X-Plex-Token.id")).strip()
    try:
      library_xml = etree.fromstring(read_url(url))
      for directory in library_xml.iterchildren('Directory'):
        for location in directory.iterchildren('Location'):
          PLEX_LIBRARY[location.get('path')] = {'title': directory.get('title'), 'scanner': directory.get("scanner"), 'agent': directory.get('agent')}
          Log.info('id: {:>2}, type: {:<6}, agent: {:<30}, scanner: {:<30}, library: {:<24}, path: {}'.format(directory.get("key"), directory.get('type'), directory.get("agent"), directory.get("scanner"), directory.get('title'), location.get("path")))
    except:  pass
  if not PLEX_LIBRARY:  Log.info("Place Plex token string in file in Plex root '.../Plex Media Server/X-Plex-Token.id' to have a log per library - https://support.plex.tv/hc/en-us/articles/204059436-Finding-your-account-token-X-Plex-Token")

  ### Read ScudLee mapping list
  global ScudLeeMappings
  ScudLeeMappings = etree.fromstring(read_cached_url(ANIDB_TVDB_MAPPING))

def read_file(local_file):
  """
  Get the contents of a file
  """
  file_content = ""
  try:
    with open(local_file, 'r') as file:
      file_content = file.read()
    
    return file_content
  except Exception as e:
    Log.error("Error reading file '%s', Exception: '%s'" % (local_file, e))
    raise e


def write_file(local_file, file_content):
  """
  Write contents to a file
  """
  try:
    with open(local_file, 'w') as file:
      file.write(file_content)
  except Exception as e:
    Log.error("Error writing file '%s', Exception: '%s'" % (local_file, e))
    raise e


def read_url(url, data=None):
  """
  Return the content returned by the URL
  """
  url_content = ""
  try:
    if data is None:
      url_content = urlopen(url, context=SSL_CONTEXT).read()
    else:
      url_content = urlopen(url, context=SSL_CONTEXT, data=data).read()
    
    return url_content
  except Exception as e:
    Log.error("Error reading url '%s', Exception: '%s'" % (url, e))
    raise e


def read_cached_url(url, filename='', cache=1 * 24 * 60 * 60):
  """
  Get the contents of a given URL. The contents are cached to a file for `cache` period after which it will be refreshed
  """
  pas_folder = os.path.join(PLEX_ROOT, 'Cache', 'PAS')
  
  # Make sure the PAS cache folder exists
  if not os.path.exists(pas_folder):
    os.mkdir(pas_folder)
  
  if not filename:  
    filename = os.path.basename(url)
  
  local_filename = os.path.join(pas_folder, filename)
  
  # Load the cached file's contents
  file_content_cache, file_age = None, cache + 1
  if os.path.exists(local_filename):
    file_content_cache = read_file(local_filename)
    file_age = time.time() - os.path.getmtime(local_filename)

  try:
    # Return the cached file string if it exists and is not too old
    if file_content_cache and file_age <= cache:
      Log.info("Using cached file - Filename: '{file}', Age: '{age:.2f} days', Limit: '{limit} days', url: '{url}'".format(file=local_filename, age=file_age / 86400, limit=cache / 86400, url=url))
      return file_content_cache
    else:
      file_content = read_url(url)
      
      # Content was pulled down so save it
      if file_content:
        Log.info("{action} cached file - Filename: '{file}', Age: '{age:.2f} days', Limit: '{limit} days', url: '{url}'".format(action="Updating" if os.path.exists(local_filename) else "Creating", file=local_filename, age=file_age / 86400, limit=cache / 86400, url=url))
        write_file(local_filename, file_content)
      return file_content
  except Exception as e:  # Exception hit from possible: xml parsing, file reading/writing, bad url call
    Log.error("Error downloading '{}', Exception: '{}'".format(url, e))
    raise e


#########################################################################################################
def winapi_path(dos_path, encoding=None): # https://stackoverflow.com/questions/36219317/pathname-too-long-to-open/36219497
    if (not isinstance(dos_path, unicode) and encoding is not None):  dos_path = dos_path.decode(encoding)
    path = os.path.abspath(dos_path)
    if path.startswith(u"\\\\"):  return u"\\\\?\\UNC\\" + path[2:]
    return u"\\\\?\\" + path

                            
### Sanitize string #####################################################################################
def os_filename_clean_string(string):
  filter_chars = "\\/:*?<>|~;"

  for char, subst in zip(list(filter_chars), [" " for x in range(len(filter_chars))]) + [("`", "'"), ('"', "'")]:    # remove leftover parenthesis (work with code a bit above)
    if char in string:  string = string.replace(char, subst)                                                         # translate anidb apostrophes into normal ones #s = s.replace('&', 'and')
  return string


#########################################################################################################  
def Dict(var, *arg, **kwarg):
  """
  Return the value of an (imbricated) dictionnary, if all fields exist else return "" unless "default=new_value" specified as end argument
  Ex: Dict(variable_dict, 'field1', 'field2', default = 0)
  """
  for key in arg:
    if isinstance(var, dict) and key and key in var:  var = var[key]
    else:  return kwarg['default'] if kwarg and 'default' in kwarg else ""   # Allow Dict(var, tvdbid).isdigit() for example
  return kwarg['default'] if var in (None, '', 'N/A', 'null') and kwarg and 'default' in kwarg else "" if var in (None, '', 'N/A', 'null') else var


### Set Logging to proper logging file ##################################################################
def set_logging(root='', foldername='', filename='', backup_count=0, format='%(message)s', mode='w'):#%(asctime)-15s %(levelname)s - 
  if Dict(PLEX_LIBRARY, root, 'agent') == 'com.plexapp.agents.hama':
    cache_path = os.path.join(PLEX_ROOT, 'Plug-in Support', 'Data', 'com.plexapp.agents.hama', 'DataItems', '_Logs')
  else:  cache_path = os.path.join(PLEX_ROOT, 'Logs', 'PAS Scanner Logs')

  if not foldername:  foldername = Dict(PLEX_LIBRARY, root, 'title')  # If foldername is not defined, try and pull the library title from PLEX_LIBRARY

  if foldername:  cache_path = os.path.join(cache_path, os_filename_clean_string(foldername))

  if not os.path.exists(cache_path):  os.makedirs(cache_path)

  filename = os_filename_clean_string(filename) if filename else '_root_.scanner.log'
  log_file = os.path.join(cache_path, filename)
  if os.sep=="\\":  log_file = winapi_path(log_file, 'utf-8') # Bypass DOS path MAX_PATH limitation

  mode = 'a' if os.path.exists(log_file) and os.stat(log_file).st_mtime + 3600 > time.time() else mode # Override mode for repeat manual scans or immediate rescans

  global Handler
  if Handler: Log.removeHandler(Handler)
  if backup_count:  Handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=10*1024*1024, backupCount=backup_count)
  else:             Handler = logging.FileHandler                 (log_file, mode=mode)
  Handler.setFormatter(logging.Formatter(format))
  Handler.setLevel(logging.DEBUG)
  Log.addHandler(Handler)


def add_episode_into_plex(media, file, root, path, show, season=1, ep=1, ep2=None, title="", mappingList={}):
  """
  Add files to Plex

  For normal seasonal episode, ie season > 0, episode and season offsets from the mapping will be taken into account before adding the episode to Plex.
  For seaon = 0 offsets from the mapping will not be taken into account and will be added to Plex based on `season` and `ep` parameters.
  """
  if ep <= 0:
    Log.error("Episode counts are not allowed to be 0 or less; Show: %s; File: %s" % (show, file))
    raise Exception
  if not os.path.exists(file):
    Log.error("Specified file does not exist. Please make sure to pass in full path to file; Show: %s; File: %s" % (show, file))
    raise Exception
  
  if title:
    title = title.title()

  # Season/Episode Offset
  if Dict(mappingList, 'episodeoffset'):
    ep = ep + int(Dict(mappingList, 'episodeoffset'))
    ep2 = ep2 + int(Dict(mappingList, 'episodeoffset')) if ep2 else None
  
  if not ep2 or ep > ep2:
    ep2 = ep  # make ep2 same as ep for loop and tests
  
  for epn in range(ep, ep2 + 1):
    # Media.Episode(Show, Season, Episode, Title, Year)
    tv_show = Media.Episode(show, season, epn, title, None)
    tv_show.display_offset = (epn - ep) * 100 / ( ep2 - ep + 1)
    
    tv_show.parts.append(file)
    media.append(tv_show)
  
  Log.info(
    '"{show}" S{season:>02d}E{episode:>03d}{range:s} "{title}" "{file}"'
    .format(
      show=show,
      season=season,
      episode=ep,
      range='    ' if not ep2 or ep==ep2 else '-{:>03d}'.format(ep2),
      title=title,
      file=os.path.basename(file)
    )
  )


def anidbTvdbMapping(AniDB_TVDB_mapping_tree, anidbid):
  """
  Get the TVDB mapping for a AniDB ID. If a mapping for the AniDB does not exist or the mapping's TVDB ID is empty an empty mapping will be returned
  """
  mappingList = {}
  for anime in AniDB_TVDB_mapping_tree.iter('anime') if AniDB_TVDB_mapping_tree is not None else []:
    if anime.get("anidbid") == anidbid and anime.get('tvdbid').isdigit():
      mappingList['episodeoffset'] = anime.get('episodeoffset') or '0'  # Either entry is missing or exists but is blank
      mappingList['defaulttvdbseason'] = anime.get('defaulttvdbseason') or '1'  # Either entry is missing or exists but is blank
      
      if mappingList['defaulttvdbseason'] == 'a':
        mappingList['defaulttvdbseason_a'] = True
        mappingList['defaulttvdbseason'] = '1'
      else:
        mappingList['defaulttvdbseason_a'] = False
      
      try:
        for season in anime.iter('mapping'):
          for episode in range(int(season.get("start")), int(season.get("end")) + 1) if season.get("offset") else []:
            mappingList['s' + season.get("anidbseason") + 'e' + str(episode)] = 's' + season.get("tvdbseason") + 'e' + str(episode+int(season.get("offset")))
          
          for episode in filter(None, season.text.split(';')) if season.text else []:
            mappingList['s' + season.get("anidbseason") + 'e' + episode.split('-')[0]] = 's' + season.get("tvdbseason") + 'e' + episode.split('-')[1]
      except Exception as e:
        Log.error("mappingList creation exception: {}, mappingList: {}".format(e, mappingList))
      else:
        Log.info("anidb: '%s', tvbdid: '%s', name: '%s', mappingList: %s" % (anidbid, anime.get('tvdbid'), anime.xpath("name")[0].text, str(mappingList)))
      
      return anime.get('tvdbid'), mappingList
  
  Log.error("-- No valid tvbdbid found for anidbid '%s'" % (anidbid))
  return "", {}


### Look for episodes ###################################################################################
def Scan(path, files, media, dirs, language=None, root=None, **kwargs): #get called for root and each root folder, path relative files are filenames, dirs fullpath
  setup()  # Call setup to get core info. If setup is already done, it just returns and does nothing.
  
  log_filename = path.split(os.sep)[0] if path else '_root_' + root.replace(os.sep, '-')
  
  ### Create *.filelist.log file ###
  set_logging(root=root, filename=log_filename+'.filelist.log', mode='w') #add grouping folders filelist
  Log.info("".ljust(157, '='))
  Log.info("Library: '{}', root: '{}', path: '{}', files: '{}', dirs: '{}'".format(Dict(PLEX_LIBRARY, root, 'title', default="no valid X-Plex-Token.id"), root, path, len(files or []), len(dirs or [])))
  Log.info("{} scan start: {}".format("Manual" if kwargs else "Plex", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f")))
  Log.info("".ljust(157, '='))

  ### Remove directories un-needed (mathing IGNORE_DIRS_RX) ###
  for subdir in dirs:
    for rx in IGNORE_DIRS_RX:
      if rx.match(os.path.basename(subdir)):
        dirs.remove(subdir)
        Log.info("# Folder: '{}' match '{}' pattern: '{}'".format(os.path.relpath(subdir, root), 'IGNORE_DIRS_RX', rx))
        break  #skip dirs to be ignored
      else:  Log.info("[folder] " + os.path.relpath(subdir, root))
  
  Log.info("".ljust(157, '='))
  Log.info("{} scan end: {}".format("Manual" if kwargs else "Plex", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f")))

  ### Logging to *.scanner.log ###
  set_logging(root=root, filename=log_filename+'.scanner.log', mode='w') #if recent or kwargs else 'w'
  Log.info("".ljust(157, '='))
  Log.info("Library: '{}', root: '{}', path: '{}', files: '{}', dirs: '{}'".format(Dict(PLEX_LIBRARY, root, 'title', default="no valid X-Plex-Token.id"), root, path, len(files or []), len(dirs or [])))
  Log.info("{} scan start: {}".format("Manual" if kwargs else "Plex", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f")))
  Log.info("".ljust(157, '='))
  
  try:
    if not path and not files and dirs:
      # We are in the ROOT folder

      # Go through all the dirs, which are collections, and force scan all the entries
      for dir_ in dirs:
        collection = os.path.basename(dir_)
        Log.info("Processing Collection %s", collection)
        entries = [os.path.join(dir_, sd) for sd in os.listdir(dir_) if os.path.isdir(os.path.join(dir_, sd)) and sd != "Other"]
        for entry in entries:
          Log.info("Processing Entry %s", entry)

          files = [os.path.join(entry, f) for f in os.listdir(entry) if os.path.isfile(os.path.join(entry, f))]
          Scan(os.path.join(collection, os.path.basename(entry)), files, media, [], language=language, root=root)

        other = [sd for sd in os.listdir(dir_) if os.path.isdir(os.path.join(dir_, sd)) and sd == "Other"]
        if other:
          other_entries = [os.path.join(dir_, "Other", sd) for sd in os.listdir(os.path.join(dir_, "Other")) if os.path.isdir(os.path.join(dir_, "Other", sd))]
          for other_entry in other_entries:
            Log.info("Processing Other Entry %s", other_entry)

            files = [os.path.join(other_entry, f) for f in os.listdir(other_entry) if os.path.isfile(os.path.join(other_entry, f))]
            Scan(os.path.join(collection, "Other", os.path.basename(other_entry)), files, media, [], language=language, root=root)

    elif path and files:  # Path to entry folder with files being all the episodes
      path_match = re.match(r'(?P<show>.+?)\s\[anidb\-(?P<anidb_id>\d+)\]', os.path.basename(path))
      if not path_match:
        Log.warning("Path did not match required Entry format %s", path)
        return
    
      anidb_id = path_match.group("anidb_id")
      Log.warning("Working with AniDB ID %s", anidb_id)
      # We are in an entry folder and have to process the files
      for file in files:
        # The file is the full path
        filename = os.path.basename(file)
        if filename:
          matches = re.match(VALID_FILE_REGEX, filename)
          if matches:
            episode2 = None
            title = matches.group("title")

            # Extract mapping information from ScudLee/anime-lists
            tvdb_id, mappingList = anidbTvdbMapping(ScudLeeMappings, anidb_id)

            if not mappingList or not tvdb_id:
              if not mappingList:
                Log.info("Couldn't find a mapping for AniDB ID %s", anidb_id)
              if not tvdb_id and mappingList:
                Log.info("Mapping for AniDB ID %s doesn't have a TVDB ID", anidb_id)
            else:
              Log.info("Matching '%s' with TVDB ID %s", path_match.group("show"), tvdb_id)
            
            show = "%s [anidb-%s]" % (path_match.group("show"), anidb_id)

            if matches.group("episode") is None:
              # A movie or something that does not have an episode number
              season = 1
              episode = 1
            else:
              # Just a normal episode of the main series or a OVA, Special, etc that has episode numbers
              season = 1
              episode = matches.group("episode")
              if len(episode.split('-')) > 1:
                # This is a multi-episode file
                Log.info("Working with a multi-episode file: %s", episode)
                episode, episode2 = episode.split("-")
                episode, episode2 = int(episode), int(episode2)
              elif episode[0].lower() == "s":
                # This is a special episode file
                Log.info("Working with a special episode: %s", episode)
                episode = int(episode[1:])
                season = 0
                
                key = "s0e%s" % episode
                if key in mappingList:
                  season, episode = re.split(r'\D', mappingList[key])[1:]
                  season, episode = int(season), int(episode)
              elif not episode[-1].isdigit():
                Log.info("Working with a multi-part file: %s", episode)
                # This is a multi-part episode file
                episode = int(episode[:-1])
              else:
                Log.info("Working with a normal episode file: %s", episode)
                episode = int(episode)

              # Check if there is a episode transform mapping where AniDB and TVDB episode numbers doesn't line up correctly
              # s1 indicates that it's a normal AniDB episodic season and not SP episodes linked to it
              key = "s1e%s" % episode
              if key in mappingList:
                season, episode = re.split(r'\D', mappingList[key])[1:]
                season, episode = int(season), int(episode)
                if season > 0:
                    season = 1

            add_episode_into_plex(media, file, root, path, show, season=season, ep=episode, ep2=episode2, title=title, mappingList=mappingList)
          else:
            Log.warning("Did not match required filename format %s", filename)
        else:
          Log.warning("Could not extract the filename from %s", file)
  except Exception:
    Log.exception('Something unexpected happended while scanning')
    raise


# Command line scanner call ###
if __name__ == '__main__':  #command line
  print("Plex Anime Scanner by jovaee")
  path  = sys.argv[1]
  files = [os.path.join(path, file) for file in os.listdir(path)]
  media = []
  Scan(path[1:], files, media, [])
  print("Files detected: ", media)
