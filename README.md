# Plex Anime Scanner (PAS)

PAS is a Plex file scanner which Plex uses to detect files and add it to libraries. The scanner determines the folder structure that can be supported and what files will be detected. PAS is designed to only support a very specific folder structure and file naming conventions which is described in lowers sections. 

## Anime Metadata 

All metedata like descriptions, ratings etc will be retrieved with the use of a MetaData Agent. Plex has a built in one but it will not always detect and pull the correct information for anime. Thus PAS specifically requires [Hama.bundle](https://github.com/ZeroQI/Hama.bundle).

PAS uses [AniDB](https://anidb.net/) IDs to match anime to [TVDB](https://thetvdb.com/) IDs. The reasoning for using AniDB IDs is that AniDB just works  better for anime than TVDB does. AniDB has the concept of sequels which anime follow whereas TVDB does not, eveything is seen as one show with multiple season and this is not how anime works. Converting to TVDB IDs allows all anime in your Plex Library to be handled better as Plex was built to use TVDB.

The convertion from AniDB to TVDB is done using mappings from [Anime-Lists](https://github.com/Anime-Lists/anime-lists).


## Conventions
Below the conventions for PAS will be explained which must be followed for the scanner to work as intended.
In [Folder Structure](#folder-structure) the required folder structure is explained and in [Episodes](#episodes) the naming conventions for episode filenames are explained.

### Folder Structure

The scanner supports only the following folder structure. If folders do not match the structure and naming conventions they will be skipped.

```
Root Folder (The folder that is added as root folder in the **Plex library**)
|
└───Collection Folder (Example: Boku no Hero Academia)
│   │
│   └─── Entry Folder (Example: Boku no Hero Academia [anidb-11739])
│   |   │-- Episode 01 (Example: [Erai-raws] Boku no Hero Academia - 01 [720p].mkv)
│   |   │-- Episode 02 (Example: [Erai-raws] Boku no Hero Academia - 02 [720p].mkv)
|   |   |-- Episode 03 (Example: [Erai-raws] Boku no Hero Academia - 03 [720p].mkv)
│   |   │   ...
|   |
|   └─── Entry Folder (Example: Boku no Hero Academia 2nd Season [anidb-12233])
│   |   │-- Episode 01 (Example: [Erai-raws] Boku no Hero Academia 2nd Season - 01 [720p].mkv)
│   |   │-- Episode 02 (Example: [Erai-raws] Boku no Hero Academia 2nd Season - 02 [720p].mkv)
|   |   |-- Episode 03 (Example: [Erai-raws] Boku no Hero Academia 2nd Season - 03 [720p].mkv)
|   |   |   ...
|   |
|   └─── ...
|   |
|   └─── Other
|       |
|       └─── Entry Folder (Example: Boku no Hero Academia - Futari no Hero [anidb-13633])
|       |   |-- Movie Episode (Example: [NoobSubs] Boku no Hero Academia - Futari no Hero [1080p])
|       |
|       └─── ...
|
└───...
```

The **Collection Folder** acts as the name suggests, the collection of multiple anime entries. There is no hard requirement for the naming of the collection folder but as a rule of thumb this is intended to be named after the over arching series name for all the entries. In the folder structure section `Boku no Hero Academia` was chosen as the collection name as it describes what all entries under it is about.

The **Entry Folder** is a folder that contains the episodes for a single season or movie. Entry folders have a hard requirement on naming, the "name" given the the folder does not matter but a `[anidb-xxx]` mapping identifier is required to be present at the end of the folder names. This is used to map that folder to a specific anime so that the correct metadata can be pulled for it. The ID that should be placed in the mapping identifier is the ID for the anime on [AniDB](https://anidb.net/). In the folder structure section `11739` is the mapping identifier ID used as from the [Boku no Hero Season 1](https://anidb.net/anime/11739) AniDB page.

**Other** is another semi-collection folder. In it any additional entries can be placed that the user does not want at the root of the collection folder. Usually these will be OVAs, Special etc.

### Episodes

Episode filenames require a specific naming convention otherwise the files will be skipped and they won't appear in your library.
The anatomy of a filename `[Erai-raws] Boku no Hero Academia 2nd Season - 01 [720p].mkv`
```
Erai-raws                           - The subber for the episode. Subber names are placed between []
Boku no Hero Academia 2nd Season    - The name of the show. This can be anything the user desires
-                                   - Separator between showname and episode and resolution. This can be ommitted for episodes which don't have episode numbers
01                                  - The episode number of the file. This can be ommitted for episodes which don't have episode numbers
720p                                - Resolution. Resolutions are placed between []
mkv                                 - Extension
```

If you want to check if your filenames are valid use the following regex. If it matches then you are good to go.
```python
\[(?P<subber>.+)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>(?:S?[0-9]{1,4}(?:\.5|[A-Z])?\-?){1,2}))?\s\[(?P<resolution>\d+p)\].(?P<extension>.+)
```

## Mapping Overrides

PAS allows overriding of the Anime-Lists TVDB mappings. Sometimes certain mappings are missing or not entirely correct and overriding them allows for an easy way to fix mapping issues. The override mapping will always take priority over the ScudLee mappings.

To use mapping overrides simply create a `anime-list-override.xml` file in your Plex `Cache` folder. The file follows the exact same formatting as the ScudLee mappings so it can be copied as a template if so desired.


## Installation

- Download [Plex Anime Scanner.py](https://bitbucket.org/jovaee/plex-anime-scanner/src/master/Plex%20Anime%20Scanner.py)
- Save into ``[...]/Plex/Library/Application Support/Plex Media Server/Scanners/Series/Plex Anime Scanner.py`` Note: ```Scanners``` and ```Series``` folder are not created by default and will need creating.
- Once the scanner is installed correctly, when creating a library you can select the custom scanner.
- Install [Hama.bundle](https://github.com/ZeroQI/Hama.bundle) plugin. Follow the installation instructionson their page.

## TODO

* Fix mulitfile logging.
* **(Optional)** Support for seasonal folders in Entry folders.

## Credit
The work done by ZeroQI and their [Absolute Series Scanner](https://github.com/ZeroQI/Absolute-Series-Scanner). PAS used this scanner as the initial framework.